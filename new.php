<?php
    require_once('classes.php');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>

    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
        crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <title>Product Add</title>
    </head>
    <body>
      <header>
          <nav>
            <div class="banner">
                <h4>Product Add</h4> 
                    <div class="liner"></div> 
            </div>      
          </nav>
        </header>
        <main>
              <div class="container">
                  <form method="GET">
                      <div class="button">
                      <button id="btn1" class="btn btn-secondary" type="submit" name="send">Save</button></br>
                      <a id="btn2" class="btn btn-secondary" href="list.php">List</a> 
                      </div>
                  <div class="row">
                         <div class="col-1">
                             <label>SKU</label>
                         </div>
                         <div class="col-75">
                             <input type="text" name="sku"/>
                            <span class="error">* <?php echo $detail->getSkuError();?></span>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-1">
                             <label> Name</label>
                         </div>
                         <div class="col-75">
                             <input type="text" name="name"/>
                             <span class="error">* <?php echo $detail->getNameError();?></span>
                         </div>
                     </div>
                     <div class="row">
                          <div class="col-1">
                              <label>Price</label>
                          </div>
                          <div class="col-75">
                              <input type="text" name="price"/>
                              <span class="error">* <?php echo $detail->getPriceError();?></span>
                          </div>  
                     </div>
                     <div class="row">
                         <div class="col-2">
                             <label >Type Switcher</label>
                         </div>
                         <div class="col-25">
                             <select name="type" id="type">
                                 <option name="type_switcher">Type switcher</option>
                                 <option name="dvd_disc" clas="select" value="dvd-disc">Dvd Disc</option>
                                 <option name="book" value="book">Book</option>
                                 <option name="Furniture" value="furniture">Furniture</option>
                            </select> 
                         </div>
                     </div>
                     <div class="row" id="dvd">
                            <div class="col-2 ">
                                <label>Size:</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="size"/>
                                <span class="error">* <?php echo $dvd->getSizeError();?></span>
                            </div>
                            <div col="col-75">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            </div>
                     </div>
                     <div class="row" id="book">
                         <div class="col-2">
                             <label>Weight:</label>
                         </div>
                         <div class="col-9">
                             <input type="text" name=weight>
                             <span class="error">* <?php echo $book->getWeightError();?></span>
                         </div>
                         <div class="col-75">
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                         </div>
                     </div>
                     <div class="row" id="furniture">
                         <div class="col-2">
                             <label>Height</label>
                         </div>
                         <div class="col-9">
                             <input type="text" name="height"/>
                             <span class="error">* <?php echo $furniture->getHeightError();?></span>
                         </div>
                         <div class="col-2">
                             <label>Width</label>
                         </div>
                         <div class="col-9">
                             <input type="text" name="width"/>
                             <span class="error">* <?php echo $furniture->getWidthError();?></span>
                         </div>
                         <div class="col-2">
                             <label>Length</label>
                         </div>
                         <div class="col-9">
                             <input type="text" name="length"/>
                             <span class="error">* <?php echo $furniture->getLengthError();?></span>
                         </div>
                         <div class="col-75">
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                         </div>
                     </div>
                  </form>
              </div>
    </main>
<script >
//  ----------type Switcher ----------
   $(function() {
    $('#furniture').hide(); 
    $('#book').hide();
    $('#dvd').hide();
    $('#type').change(function(){
        if($('#type').val() == 'furniture') {
            $('#furniture').show(); 
            $('#book').hide();
            $('#dvd').hide();
        } 
       else if($('#type').val()=='book') {
          $('#book').show();
          $('#furniture').hide();
          $('#dvd').hide();
        }
        else if($('#type').val()=='dvd-disc') {
          $('#book').hide();
          $('#furniture').hide();
          $('#dvd').show();
        }
        else {
          $('#book').hide();
          $('#furniture').hide();
          $('#dvd').hide();
        }
    });
});
</script>
</body>
</html>
      