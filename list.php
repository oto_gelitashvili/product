<?php
require('classes.php');
require ('db.php');
$conn=mysqli_connect("localhost","root","","product_list");
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>

    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
        crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <title>Product List</title>
    </head>
    <body>
      <header>
            <nav>  
                <div class="banner">
                <h4>Product List</h4> 
             <div class="liner"></div> 
                </div>
          </nav>
      </header>
      <main>
        <form method="post" >
            <div class="button-2">
              <a id="btn4" class="btn btn-secondary" href="new.php">New</a>
              <button id="btn3" class="btn btn-secondary" type="submit" name="delete" >apply</button>
             </div>
            <div class="dropdown" >
              <button id="dropdown" class="btn btn-secondary dropdown-toggle" id="drop" type="button"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
              Mass Delete Action</button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Action</a></div>
           </div>
            <br></br>
           <div class="container-2">
              <div class="row">
                    <?php 
                    $query = "SELECT * FROM Book"; 
                    $result = mysqli_query($conn,$query); 
                     // ---- each row
                     while($row = mysqli_fetch_array($result) ){
                      $detail->setId( $row['id'] );
                      $detail->setSku ($row['sku'] );
                      $detail->setName( $row['name'] );
                      $detail->setPrice( $row['price'] );
                      $book->setWeight( $row['weight'] );
                    ?>
 
                  <div class="column">
                      <div class="card">
                          <div class="form-check" >
                             <input type="checkbox"  name="checkbox[]" value='<?= $detail->getId();?>'>
                          </div>  
                        <!-- insert data -->
                        <h5><?php echo $detail->getSku();?></h5>
                        <h5><?php echo $detail->getName();?></h5>
                        <h5><?php echo  number_format($detail->getPrice(),2)." $"?></h5>
                        <h5><?php echo"Weight:  ". $book->getWeight() . " KG"; ?></h5>
                      </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <?php 
                      $query = "SELECT * FROM Dvd_disc";
                      $result= mysqli_query($conn,$query);
                      // output data of each row
                      while($row = mysqli_fetch_array($result))  {
                        $detail->setId($row['id']);
                        $detail->setSku( $row['sku'] );
                        $detail->setName( $row['name'] );
                        $dvd->setSize( $row['size'] );
                        $detail->setPrice( $row['price'] );
                     ?>  
                   <div class="column">
                      <div class="card">
                        <div class="form-check">
                            <input type="checkbox" name="checkbox[]" value='<?= $detail->getId()?>'>
                         </div>
                        <!-- insert data -->
                       <h5><?php echo $detail->getSku();?></h5>
                       <h5><?php echo $detail->getName();?></h5>
                       <h5><?php echo  number_format($detail->getPrice(),2)." $"?></h5>
                       <h5><?php echo"Size:  ". $dvd->getSize() . " MB"; ?></h5>
                      </div>
                    </div>
                    <?php } ?> 
                  </div>
                  <div class="row">
                         <?php 
                         $conn=mysqli_connect("localhost","root","","product_list");
                            $sql = "SELECT * FROM Furniture";
                            $result= $conn->query($sql);
                            // output data of each row
                            while($row = mysqli_fetch_array($result))  {
                             $detail->setId($row['id']);
                             $detail->setSku( $row['sku'] );
                             $detail->setName( $row['name'] );
                             $furniture->setWidth( $row['width'] );
                             $furniture->setHeight( $row['height'] );
                             $furniture->setLength( $row['length'] );
                             $detail->setPrice( $row['price'] );
                          ?>  
                        <div class="column">
                             <div class="card">
                                  <div class="form-check">
                                     <input type="checkbox"  name="checkbox[]" value='<?= $detail->getId()?>'>
                                   </div>
                                <!-- insert data -->
                               <h5><?php echo $detail->getSku();?></h5>
                               <h5><?php echo $detail->getName();?></h5>
                               <h5><?php echo  number_format($detail->getPrice(),2)." $"?></h5>
                               <h5><?php echo"Dimensions:". $furniture->getHeight() ."x".$furniture->getWidth(). "x" .$furniture->getLength(); ?></h5>
                              </div>
                          </div>
                          <?php } ?> 
                        </div>
            </div>
          </form>
      </main>
    </body>
</html>