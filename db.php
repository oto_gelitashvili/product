<?php
require_once('classes.php');
// ---Create Objects----
  $db=new Database();
  $detail =new Detail();
  $book=new Book();
  $dvd =new Dvd();
  $furniture=new Furniture();
// ----- send data to product list
if( isset($_GET['send'])) {
  $detail->setSku($_GET['sku']);
  $detail->setName($_GET['name']);
  $detail->setPrice( $_GET['price'] );
  
  $book->setWeight( $_GET['weight'] );
  
  $dvd->setSize( $_GET['size'] );
  
  $furniture->setLength( $_GET['length']);
  $furniture->setWidth( $_GET['width'] );
  $furniture->setHeight( $_GET['height'] );
  
}
  //--- validate data
if( empty($detail->getSku())||empty($detail->getName())  || empty($detail->getPrice()) || empty($book->getWeight()) || 
empty($dvd->getSize()) || empty($furniture->getHeight()) || empty($furniture->getLength()) || empty($furniture->getWidth())){
  $detail->setSkuError( "SKU is Required" );
  $detail->setNameError( "Name is Required" );
  $detail->setPriceError( "Price is Required" );
  $book->setWeightError ( "Weight is Required" );
  $dvd->setSizeError( "Size is Required" );
  $furniture->setLengthError( "Length is Required" );
  $furniture->setWidthError( "Width is Required" );
  $furniture->setHeightError( "Height is Required" );
}

// -------- if name is number
 if(is_numeric($detail->getName()))
{
  $detail->setNameError( "Name must be from letters" );
}

if(preg_match("/^([a-zA-Z' ]+)$/",$detail->getPrice()) || preg_match("/^([a-zA-Z' ]+)$/",$book->getWeight()) || preg_match("/^([a-zA-Z' ]+)$/",$dvd->getSize())||  preg_match("/^([a-zA-Z' ]+)$/",$furniture->getLength())||
preg_match("/^([a-zA-Z' ]+)$/",$furniture->getWidth()) ||  preg_match("/^([a-zA-Z' ]+)$/",$furniture->getHeight()))
{
  $detail->setPriceError("Price must be a number");
  $book->setWeightError( "weight must be a number" );
  $dvd->setSizeError( "Size must be a number" );
  $furniture->setLengthError( "Length must be a number" );
  $furniture->setHeightError( "Height must be a number" );
  $furniture->setWidthError( "Width must be a number" );
}


if($book->getWeight()<0) {
  $book->setWeightError( "Weight must be greater than 0" );
}
if($dvd->getSize()<0){
  $dvd->setSizeError( "Size must be greater than 0" );
}
if($furniture->getLength() < 0){
  $furniture->setLengthError( "Length must be greater than 0" );
}

if($furniture->getHeight()<0){
  $furniture->setHeightError("Heght must be greater than 0" );
}
if($furniture->getWidth()<0){
  $furniture->setWidthError( "Width must be greater than 0" );
}

//  ------if evrythig is okay

  if($detail->getSku()!=null  && $detail->getName()!=null && $detail->getPrice()>0 )  {
    if($book->getWeight()>0 ){
      $db->insertBook($detail->getSku(), $detail->getName(), $detail->getPrice(), $book->getWeight());
    }
  if($dvd->getSize()>0) {
    $db->insertDvd($detail->getSku(), $detail->getName(), $detail->getPrice(), $dvd->getSize());
   }
   if( $furniture->getLength()>0 && $furniture->getWidth()>0 && $furniture->getHeight()>0){
  $db->insertFurniture($detail->getSku(), $detail->getName(), $detail->getPrice(), $furniture->getWidth(),$furniture->getHeight(),$furniture->getLength());
   }
  }


  // --------Delete method -------
  
  if(isset($_POST['delete']))
  {
    $conn=mysqli_connect("localhost","root","","product_list");
    if(isset($_POST['checkbox']))
    {
    foreach ($_POST['checkbox'] as $id){
      $conn->query("DELETE FROM Book WHERE id=".$id);
    }
    foreach ($_POST['checkbox'] as $id){
      $conn->query("DELETE FROM Dvd_disc WHERE id=".$id);
    } 
    foreach ($_POST['checkbox'] as $id){
      $conn->query("DELETE FROM Furniture WHERE id=".$id);
    }
  }   
    header("Location:list.php");
  }

?>