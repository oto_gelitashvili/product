<?php
require 'db.php';

class Database{
    public function __construct(){
        // ---create connection----
        $conn= mysqli_connect("localhost","root","");
        // --create database--
        mysqli_query($conn,"CREATE DATABASE product_list");
        // -- use database--
        mysqli_select_db($conn,"product_list");
        // -- create tables--
         $sql_1 = "CREATE TABLE Book (
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            sku VARCHAR(50),
            name VARCHAR(50),
            price float(50),
            weight VARCHAR(50)
         )";
        $sql_2 = "CREATE TABLE Dvd_disc (
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            sku VARCHAR(50),
            name VARCHAR(50) ,
            price float(50),
            size int(50)
            )";
        $sql_3 = "CREATE TABLE Furniture (
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            sku VARCHAR(50),
            name VARCHAR(50) ,
            price float(50),
            width int(50),
            length int(50),
            height int(50)
            )";
         
         mysqli_query($conn,$sql_1);
         mysqli_query($conn,$sql_2);
         mysqli_query($conn,$sql_3);
         mysqli_query($conn,"ALTER TABLE Dvd_disc AUTO_INCREMENT=100");
         mysqli_query($conn,"ALTER TABLE Furniture AUTO_INCREMENT=200");
        }
        // --insert data of Books
    public function insertBook( $sku, $name,$price,$weight)
    {
        $string= mysqli_query(mysqli_connect("localhost","root","","product_list"),"INSERT INTO Book (sku,name,price,weight) VALUES ('$sku','$name','$price','$weight')");
        if($string===true){
        header("Location:new.php");}
        return $string;
    }
    // --insert data of Dvds
    public function insertDvd( $sku, $name,$price,$size)
    {
        $string= mysqli_query(mysqli_connect("localhost","root","","product_list"),"INSERT INTO Dvd_disc (sku,name,price,size) VALUES ('$sku','$name','$price','$size')");
        if($string===true){
        header("Location:new.php");}
        return $string;
    }
    // --insert data of furnitures
    public function insertFurniture( $sku, $name,$price,$width,$height,$length)
    {
        $string= mysqli_query(mysqli_connect("localhost","root","","product_list"),"INSERT INTO Furniture (sku,name,price,width,height,length) 
        VALUES ('$sku','$name','$price','$width','$height','$length')");
        if($string===true){
        header("Location:new.php");}
        return $string;
    }
}
class Detail{ 
    private $name;
    private $sku;
    private $price;
    private $id;

    function setId($id){return $this->id=$id; }
    function getId(){return $this->id; }
    function setName($name){ return $this->name=$name;}
    function getName(){ return $this->name; }
    function setSku($sku){ return $this->sku=$sku;}
    function getSku(){ return $this->sku; }
    function setPrice($price){ return $this->price=$price;}
    function getPrice(){ return $this->price; }
    //----- Errors----
    private $nameError;
    private $skuError;
    private $priceError;

    function setNameError($nameError){ return $this->nameError=$nameError;}
    function getNameError(){ return $this->nameError; }
    function setSkuError($skuError){ return $this->skuError=$skuError;}
    function getSkuError(){ return $this->skuError; }
    function setPriceError($priceError){ return $this->priceError=$priceError;} 
    function getPriceError(){ return $this->priceError; }
}
class Book extends Detail{
    private $weight;
    private $id;

    function setId($id){return $this->id=$id; }
    function getId(){return $this->id; }
    function setWeight($weight){ return $this->weight=$weight; }
    function getWeight(){ return $this->weight; }
    // ----Error----
    private $weightError;
    
    function setWeightError($weightError) { return $this->weightError=$weightError; }
    function getWeightError() { return $this->weightError; }
}
class Furniture extends Detail {
    private $length;
    private $width;
    private $height;
    private $id;

    function setId($id){return $this->id=$id; }
    function getId(){return $this->id; }
    function setLength($length){return $this->length=$length; }
    function getLength(){return $this->length; }
    function setWidth($width){return $this->width=$width; }
    function getWidth(){return $this->width; }
    function setHeight($height){return $this->height=$height; }
    function getHeight(){return $this->height; }
    // ----Errors----
    private $lengthError;
    private $widthError;
    private $heightError; 

    function setLengthError($lengthError){return $this->lengthError=$lengthError; }
    function getLengthError(){return $this->lengthError; }
    function setWidthError($widthError){return $this->widthError=$widthError; }
    function getWidthError(){return $this->widthError; }
    function setHeightError($heightError){return $this->heightError=$heightError; }
    function getHeightError(){return $this->heightError; }
}
class Dvd extends Detail{
    private $size;
    private $id;

    function setId($id){return $this->id=$id; }
    function getId(){return $this->id; }
    function setSize($size){ return $this->size=$size; }
    function getSize(){ return $this->size; }
    //---- Error-----
    private $sizeError;

    function setSizeError($sizeError){ return $this->sizeError=$sizeError; }
    function getSizeError(){ return $this->sizeError; }
}
?>
